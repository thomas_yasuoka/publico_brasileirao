#this is the new scraping, from the new table we extracted from the srgoool website (manually)

#get the libraries
import pandas as pd
import re


#define main()
#def main():


#SECTION 1: getting the dfs

def get_all(): #write function to read all the htmls we'll have downloaded
    list_dfs_temp = []
    for i in range(9):
        index = i + 1
        if index < 10:
            index_string = "0" + str(index)
        else:
            index_string = str(index)
        df_temp = pd.read_html("goool%s.html" %index_string)[197]
        clean(df_temp)
        list_dfs_temp.append(df_temp) #appends all the wanted dataframes into the list, [197] indicates the dataframe we want from all the datframes in the html
        #something range starts with 0, so for the first round we need i + 1
    
    return list_dfs_temp
    
def clean(df): #write function that cleans the data
    df.dropna(axis = 1, how = "all", inplace = True) #drops "unnamed" columns
    df.dropna(axis = 0, how = "all", inplace = True) #drops last row, NaNs
    

#SECTION 2: DF data gathering section

##SUBSECTION 1: getting the attendance numbers

def att_all(dfs_list):
    attendance_temp = []
    
    for i in dfs_list:
        attendance_temp.append(att(i))
        
    return attendance_temp

def att(df):
    i = 0
    list_temp = []
    
    while i < 22:
        list_temp.append(df.iloc[i]["PP"])
        i += 3
    
    for i, value in enumerate(list_temp):
        list_temp[i] = int(list_temp[i].replace(".", ""))
        
    return list_temp
    
##SUBSECTION 2: getting the team names

def home_all(dfs_list):
    home_temp = []
    
    for i in dfs_list:
        home_temp.append(home(i))
        
    return home_temp

def home(df):
    i = 1
    list_temp = []
    
    while i < 23:
        list_temp.append(df.iloc[i]["Jogo"])
        i += 3
    
    return list_temp
    
    
def away_all(dfs_list):
    away_temp = []
    
    for i in dfs_list:
        away_temp.append(away(i))
        
    return away_temp
    
def away(df):
    i = 1
    list_temp = []
    
    while i < 23:
        list_temp.append(df.iloc[i]["I"])
        i += 3
    
    return list_temp
    
#SUBSECTION 3: getting the results

def home_and_away_goals(all_results_list):
    home_goals_temp = []
    away_goals_temp = []
    
    for i in all_results_list:
        home_goals_temp.append(home_goals(i))
        away_goals_temp.append(away_goals(i))
        
    return home_goals_temp, away_goals_temp
        
  
def get_results_all(dfs_list):
    result_list_temp = []
    
    for i in dfs_list:
        result_list_temp.append(get_results(i))
        
    return result_list_temp

def get_results(df):
    i = 1
    result_list_temp = []
    while i < 23:
        result_list_temp.append(df.iloc[i]["PP"])
        i += 3
        
    return result_list_temp
        
        
def home_goals(results):
    goals_temp = []
    
    for i in results:
        goals_temp.append(int(i[0]))
        
    return goals_temp
    
    
def away_goals(results):
    goals_temp = []
    
    for i in results:
        goals_temp.append(int(i[-1]))
        
    return goals_temp
    
def points(home_goals, away_goals):
    points_list = []
    rounds_points_list = []
    
    for i, round in enumerate(home_goals):
        for j, goals in enumerate(round):
            if goals > away_goals[i][j]:
                rounds_points_list.append(3)
            elif goals == away_goals[i][j]:
                rounds_points_list.append(1)
            else:
                rounds_points_list.append(0)
        points_list.append(rounds_points_list.copy())
        rounds_points_list.clear()
        
    return points_list
                
        
    
#SECTION 3: df making section
#So we'll make 9 dfs of each round, each one containing: Home team name, away team name, goals home, goals away, points, attendance
def now(): #Home team name, away team name, goals home, goals away, points, attendance
    a = get_all()
    
    b = home_all(a)
    c = away_all(a)
    d, e = home_and_away_goals(get_results_all(a))
    f = points(d, e)
    g = att_all(a)
    return b, c, d, e, f, g
    
def df_maker(a):
    home_team, away_team, home_goals, away_goals, points, attendance = now()
    dfs_list_temp = []
    
    for i, value in enumerate(home_team):
        dict_temp = {"Casa" : home_team[i], "Fora" : away_team[i], "Gols Casa" : home_goals[i], "Gols Fora" : away_goals[i], "Pontos" : points[i], "Público Pagante" : attendance[i]}
        dfs_list_temp.append(pd.DataFrame(dict_temp.copy()))
        dict_temp.clear()
    
    return dfs_list_temp, pd.concat(dfs_list_temp)
